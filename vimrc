execute pathogen#infect()
filetype plugin indent on
set number
set tabstop=4
set shiftwidth=4
set hlsearch

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"""" BufferGator
let g:buffergator_viewport_split_policy = 'B'
nmap <F4> :BuffergatorToggle<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"""" Colors
syntax on
set t_Co=256
colorscheme jellybeans

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"""" Cscope
function! LoadCscope()
	let db = findfile("cscope.out", ".;")
	if (!empty(db))
		let path = strpart(db, 0, match(db, "/cscope.out$"))
		set nocscopeverbose " suppress 'duplicate connection' error
		exe "cs add " . db . " " . path
		set cscopeverbose
	endif
endfunction

if $CSCOPE_DB != ""
	cs add $CSCOPE_DB
else
	au BufEnter /* call LoadCscope()
endif

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"""" Ctags
" Look for tags up to root dir.
:set tags=tags;/ 

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"""" CtrlP
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'
let g:ctrlp_working_path_mode = 'ra'
let g:ctrlp_max_files = 0
" Ignore these directories.
set wildignore+=*/tmp/*,*.so,*.swp,*.zip
" Delegate to another searcher.
" let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'
" let g:ctrlp_custom_ignore = '\v[\/]\.(git|hg|svn)$'
let g:ctrlp_match_window = 'bottom,order:btt,min:1,max:60'
let g:ctrlp_user_command = ['.git', 'cd %s && git ls-files . -co --exclude-standard', 'find %s -type f']

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"""" Nerdtree
" Show if no file opened.
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
" Shortcut.
nmap <F3> :NERDTreeToggle<CR>
" Exit vim if no more files open.
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"""" Paste
" Toggle paste mode and disable before pasting already indented text.
nnoremap <F2> :set invpaste paste?<CR>
set pastetoggle=<F2>
set showmode

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"""" Syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

" Disable by default, and run when desired.
let g:syntastic_mode_map = { 'mode': 'passive', 'active_filetypes': [],'passive_filetypes': [] }
nnoremap <C-w>E :SyntasticCheck<CR> :SyntasticToggleMode<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"""" Tagbar
nmap <F8> :TagbarToggle<CR>
